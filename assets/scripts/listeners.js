/**
 * Buttons click monitoring
 */
$spanAlphabet.forEach(($spanAlpha) => {

    $spanAlpha.addEventListener('click', () => {
        addClass(checkletter($spanAlpha.textContent), $spanAlpha);
        checkWin(clickCounter);
    })
});

/**
 * Monitoring input
 */
$wordInput.addEventListener('keyup', function (event) {
    if (event.keyCode === 13) {
        let proposition = $wordInput.value;
        checkWord(proposition);
    }
});

/**
 * Monitoring touch
 */
$body.addEventListener('keydown', function (Event) {
    let touch = Event["key"].toUpperCase();
    let classe = checkletter(touch);

    addClass(classe, null, touch);
    checkWin(clickCounter);
})

/**
 * Monitors the validation of the difficulty level
 */
$validBtn.addEventListener('click', () => {
    level = document.querySelector('#inputDico').value;
    allowStartGame(level);
    closeModal();
});