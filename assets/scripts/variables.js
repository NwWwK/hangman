/**
 * Dico easy
 * @type {string[]}
 */
const dicoEasy = [
    'TOTO',
    'TATA',
    'TUTU',
    'JOJO'
];

/**
 * Dico medium
 * @type {string[]}
 */
const dicoMedium = [
    'MARTINI',
    'VODKA',
    'WHISKY'
];

/**
 * Dico Hard
 * @type {string[]}
 */
const dicoHard = [
    'WAGNER',
    'CHOPIN',
    'MOZART',
    'BEETHOVEN',
    'BACH'
];

/**
 * Input
 * @type {Element}
 */
let $wordInput = document.querySelector('#inputTest');

/**
 * Select .capture
 * @type {Element}
 */
let $divCapture = document.querySelector('.capture');

/**
 * @type {string[]}
 */
const responseClasses = [
    'true',
    'false'
];

/**
 * List of span Alphabet
 * @type {NodeListOf<Element>}
 */
const $spanAlphabet = document.querySelectorAll('.alphabet > span');

/**
 * Span number test
 * @type {Element}
 */
const $totalTest = document.querySelector('.testing');

/**
 * Keep the number of click done on the grid
 * @type {number}
 */
let clickCounter = 0;

/**
 * Word to guess
 * @type {string}
 */
let word = '';

/**
 * Select the body tag
 * @type {HTMLBodyElement}
 */
$body = document.querySelector('body');

/**
 * Select the valid button (modal)
 * @type {Element}
 */
const $validBtn = document.querySelector('.valide');

/**
 * Select the parent div of the modal
 * @type {Element}
 */
const $modal = document.querySelector('#info');