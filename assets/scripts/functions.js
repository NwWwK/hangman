/**
 * Start the game
 */
function allowStartGame(level) {
    //Reset variable
    window.localStorage.removeItem('dico');
    window.localStorage.removeItem('word');
    $wordInput.value = '';
    clickCounter = 0;

    // Choice new word
    word = choiceWord(level);

    // Reset clickCounter
    updateClickCounter(clickCounter);

    // Remove css classes
    clearButton();

    // Display underscore
    displayLine(word.length);

    //Open the modal to choose the level of difficulty
    openModal();
}

/**
 * Remove button class
 */
function clearButton() {
    for (let $spanAlpha of $spanAlphabet) {

        for (let cssClass of responseClasses) {
            if ($spanAlpha.classList.contains(cssClass)) {
                $spanAlpha.classList.remove(cssClass)
            }
        }
    }
}

/**
 * Generate a random number
 * @param max
 * @returns {number}
 */
function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

/**
 * Select a word from the dictionary randomly
 * @returns {string}
 */
function choiceWord(level) {
    let levelDico = '';

    // Affects the chosen dictionary
    switch (level) {
        case '1':
            levelDico = dicoEasy;
            break;

        case '2':
            levelDico = dicoMedium;
            break;

        case '3':
            levelDico = dicoHard;
            break;

        default:
            alert('erreur');
            allowStartGame();
    }

    // Get the number of words in the array
    let nbWord = levelDico.length;

    // Generates a random index
    let indexWord = getRandomInt(nbWord);

    // Assignment of the word to guess
    word = levelDico[indexWord];

    // Check if the word is the same as the previous part ->  Not finished because only checks if the last word has been used
    while (word === localStorage.getItem('word', word)) {
        indexWord = getRandomInt(nbWord);
        word = dicoEasy[indexWord];
    }

    // Storing the word in localstorage
    window.localStorage.setItem('word', word);

    // For information during testing
    console.log(word);
    console.log(levelDico);

    return word;
}

/**
 * Displays underscores according to the number of letters
 * @param length
 */
function displayLine(length) {

    // Creation and display of underscores
    for (let i = 0; i < length; i++) {

        // Creation and assignment of span + underscore
        const $p1 = document.createElement('span');
        const $mystere = document.createTextNode(' _ ');

        $divCapture.appendChild($p1);
        $p1.appendChild($mystere);
    }
}

/**
 * Update counter on the DOM
 * @param clickCounter
 */
function updateClickCounter(clickCounter) {
    $totalTest.innerHTML = clickCounter;
}

/**
 * Checks the presence of a letter in the word to be defined
 * @param letter
 * @returns {string}
 */
function checkletter(letter) {

    //Declaration of constants
    const arrayWord = word.split('');
    const indexWord = [];

    // If letter missing
    if (word.split(letter).length - 1 === 0) {

        //Update clickCounter
        clickCounter++;
        updateClickCounter(clickCounter);

        // Drawing display
        visibityDead();

        // Return class FALSE css
        return 'false'

    } else {
        // For each letter in the array Word
        for (let item in arrayWord) {

            // If the letter is present in the table
            if (arrayWord[item] === letter) {
                // Index storage
                indexWord.push(item);
            }
        }
        // Replacement of the underscore by the correct letter
        displayLetter(letter, indexWord);

        // Return class TRUE css
        return 'true'
    }
}

/**
 * Replacement of the underscore by the letter
 * @param letter
 * @param indexWord
 */
function displayLetter(letter, indexWord) {
    //Selecting span tags
    const $span = document.querySelectorAll('.capture > span');

    // Replacing underscores with a letter
    for (let index in indexWord) {
        $span[indexWord[index]].innerHTML = letter;
    }

    // Information message
    alert('Nouvelle lettre trouvée : ' + letter);
}

/**
 * Assign the css class "TRUE" or "FALSE" on the button
 * @param $span
 * @param classe
 * @param letter
 */
function addClass(classe, $span = null, letter = null) {

    // If no span tag transmitted
    if ($span === null) {

        // Search for the tag
        for (let item of $spanAlphabet) {

            // If the tag is equal to the letter entered
            if (item.textContent === letter) {
                // Tag assignment
                $span = item;
            }
        }
    }

    // Adding the class
    $span.classList.add(classe);
}

/**
 * Check if the game continues or ends
 * @param clickCounter
 */
function checkWin(clickCounter) {

    //  If the number of trials is equal to or greater than 10
    if (clickCounter >= 10) {

        // Lost game, information message
        alert('Vous avez perdu ! Le mot a trouvé était :' + word);

        // Refresh the page
        location.reload();

    } else {
        // Declaration of variables and retrieval of selectors
        const resp = [];
        const $span = document.querySelectorAll('.capture > span');

        //Retrieving letters displayed on the screen and formatting a array
        for (let letter of $span) {
            resp.push(letter.innerText);
        }

        // Concatenation of values
        const proposition = resp.join("");

        // Check if the words are the same
        if (word === proposition) {
            alert('Vous avez gagné la partie !');
            location.reload();
        }
    }
}

/**
 * Direct verification of the word entered in the input
 * @param proposition
 */
function checkWord(proposition) {
    // If the proposal is correct
    if (proposition === word) {

        //Information message
        alert('Vous avez gagné !');

        // Refresh the page
        location.reload();
    } else {

        // Adding a new test to the counter and updating the counter
        clickCounter++
        updateClickCounter(clickCounter);

        //Information message
        alert('Reponse fausse. Veuillez réessayer !')

        // Drawing display
        visibityDead();
    }
}

/**
 * Display hangman SVG drawing areas
 */
function visibityDead() {
    const $socle = document.querySelector('#socle');
    const $poutre = document.querySelector('#poutre');
    const $topbar = document.querySelector('#topbar');
    const $renfort = document.querySelector('#renfort');
    const $corde = document.querySelector('#corde');
    const $tete = document.querySelector('#tete');
    const $corps = document.querySelector('#corps');
    const $bras = document.querySelector('#bras');
    const $jgauche = document.querySelector('#jgauche');
    const $jdroite = document.querySelector('#jdroite');

    if ($socle.style.display !== 'inline') {
        $socle.style.display = 'inline';
    } else if ($poutre.style.display !== 'inline') {
        $poutre.style.display = 'inline';
    } else if ($topbar.style.display !== 'inline') {
        $topbar.style.display = 'inline';
    } else if ($renfort.style.display !== 'inline') {
        $renfort.style.display = 'inline';
    } else if ($corde.style.display !== 'inline') {
        $corde.style.display = 'inline';
    } else if ($tete.style.display !== 'inline') {
        $tete.style.display = 'inline';
    } else if ($corps.style.display !== 'inline') {
        $corps.style.display = 'inline';
    } else if ($bras.style.display !== 'inline') {
        $bras.style.display = 'inline';
    } else if ($jgauche.style.display !== 'inline') {
        $jgauche.style.display = 'inline';
    } else if ($jdroite.style.display !== 'inline') {
        $jdroite.style.display = 'inline';
    } else {
        alert('Erreur, fin de la partie. Veuillez réessayer !');

        // Refresh the page
        location.reload();
    }
}

/**
 * Open Modal CSS
 */
function openModal() {
    $modal.classList.add('open');
}

/**
 * Close modal CSS
 */
function closeModal() {
    $modal.classList.add('closed');
}